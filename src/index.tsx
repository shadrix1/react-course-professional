// Core
import React from 'react';
import { render } from 'react-dom';

/* Components */
import { HomePage } from './pages/home';

/* Instruments */
import './theme/main.scss';

render(<HomePage />, document.getElementById('root'));

// import './_examples/1-react-without-jsx';
